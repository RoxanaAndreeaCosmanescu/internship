// Module dependencies.
var express = require("express")
  , http    = require("http")
  , path    = require("path")
  , routes  = require("./Work/routes");
var app     = express();

// All environments
//setting port and ip to run the application locally and remote
var port = process.env.OPENSHIFT_NODEJS_PORT || 3000;
var ip = process.env.OPENSHIFT_NODEJS_IP || "127.0.0.1";
app.set("port", port);
app.set("views", __dirname + "/Work/views");
app.set("view engine", "ejs");
app.use(express.favicon());
app.use(express.logger("dev"));
app.use(express.bodyParser());
app.use(express.methodOverride());
app.use(express.cookieParser("61d333a8-6325-4506-96e7-a180035cc26f"));
app.use(express.session());
app.use(app.router);
app.use(express.static(path.join(__dirname+"/Work/", "public")));
app.use(express.errorHandler());
var errorhandler = require('errorhandler');

    if (app.get('env') === 'development') {
    app.use( errorhandler() );
}

// App routes
// http....com/:categ1/
// http....com/:categ1/:categ2
// http....com/:categ1/:categ2/:categ3
app.get("/shop", routes.index);
app.get("/convert",routes.convert);
app.get("/orders",routes.orders);
app.get("/shop/:categ", routes.category);
app.get("/shop/:categ/:subcateg",routes.subcategory);
app.get("/shop/:categ/:subcateg/:products",routes.products);
app.get("/shop/:categ/:subcateg/:products/:prod",routes.product_description);
app.get("/wishlist",routes.wishlist);
app.get("/addToWishlist",routes.addToWishlist);
app.get("/removeFromWishlist",routes.remove_wishlist);
app.post("/loginVerification",routes.login);
app.post("/signup",routes.sign_up);
app.post("/logout",routes.logout);
app.post("/resetPassword",routes.reset_password);
app.post("/emailVerification",routes.email_verification);
app.post("/orderHistory",routes.order_history);
app.post("/cardPayment", routes.cardPayment);

// Run server
http.createServer(app).listen(app.get("port"),ip, function() {
	console.log("Express server listening on port " + app.get("port"));
});
