(function () {
		var previous, oldPrice;
		$('#currencySelector').on('focus', function () {
			previous = this.value;
			oldPrice=$('#priceContainer').html();
		});
		$('#currencySelector').on('change',function() {
			var newCurrency=$(this).val();
			$.get("/convert",{from:previous,to:newCurrency,price:oldPrice},function(response){
				$('#priceContainer').html(response.newPrice.toFixed(2));
				oldPrice=response.newPrice;
			});
			// Make sure the previous value is updated
			previous = this.value;
		});
})();