var _ = require("underscore");
var MD5=require('MD5');
var showMoreInfo=false;
var basic_info;
var complete_description;
var limit= 20; //limit used for the number of products displayed on the page
var nodemailer = require('nodemailer'); //module for email sending
/*transporter is the object able to send emails*/
var transporter = nodemailer.createTransport({
    service: 'gmail',
    auth: {
        user: 'clothingnew.nodejs@gmail.com',
        pass: 'nodejslover'
    }
});
/* function that transforms a string like "mens-clothing-suits" into  "Suits", extract the last word of the string and capitalize it, used in breadcrumb and categories name for a better appearance"*/
function reduce_capitalize(composed_word){
	var arr=composed_word.split("-");
	var len=arr.length-1;
	return arr[len].charAt(0).toUpperCase()+arr[len].substring(1);
};
/*calculates length of description and set showMoreInfo to true if the description' s length is bigger than 200 ("more info" button will be displayed; otherwise set showMoreInfo to false*/
function description_len(long_description, short_description)
{
	/*if descriptions are different product description is obtain by concatenation between descriptions*/
	if(long_description!=short_description)
	{
		complete_description=long_description+" "+short_description;
	}
	else
	{
		/*otherwise description is one if the two descriptions*/
		complete_description=long_description;
	}
	/*compare description length to 200 and set showMoreInfo*/
	if(complete_description.length>200)
	{
		showMoreInfo=true;
	}
	else
		showMoreInfo=false;
}
/*if description is longer than 200 characters it will be splitted in 2 parts, basic description and details that will appear at "more info" button click*/
function setDescription(description)
{
	if(showMoreInfo==true)
	{
		basic_info=description.substr(0,200); //basic info
	}

}
/*calculates value for the Next button*/
function calculateNext(oldValue){
	var newValue=oldValue*1+limit;
	return newValue;
};
/*calculates value for the Prev button*/
function calculatePrev(oldValue,showPrev){
	var newValue=oldValue*1-limit;
	if(newValue>0)
	{ 
		showPrev=true;
		return newValue;
	}
	else
		return 0;
};

//function which shows a page with an error message
function showError(res,error_message)
{
	res.render('error', {
		error_message: error_message,
		title: "Error"
	});
};
/* var isRelease is a boolean variable
	if isRelease is 1 then process.env.OPENSHIFT_NODEJS_PORT is set (the application is running in remote mode)
	if isRelease is 0 then process.env.OPENSHIFT_NODEJS_PORT is undefined (the application is running locally)
*/
var isRelease = process.env.OPENSHIFT_NODEJS_PORT != undefined;
var mongoConnection = isRelease? "mongodb://127.3.162.130:27017/admin" :"mongodb://localhost:27017/shop";
/* mongoUser and mongoPassword are information privided by Openshift needed to authenticate to the database*/
var mongoUser = "admin"
var mongoPassword = "yAjmUDuXpJzd";
console.log("Currently using db configuration: %s",mongoConnection);
/*
function connectToDb realize the database connection
if the application  does not run locally then is neccessary to authenticate using information provided by Openshift
*/
function connectToDb(cb){
	var mdbClient = require('mongodb').MongoClient;
	mdbClient.connect(mongoConnection, function(err, db) {
		if(isRelease){
			console.log("trying to authenticate with user %s and password %s",mongoUser,mongoPassword);
			db.authenticate(mongoUser, mongoPassword, function(err, res) {
				 if(!err) {
					console.log("Authenticated");
				} else {
					console.log("Error in authentication.");
					console.log(err);
				}
				cb(err,db);
			})
			
		}
		else cb(err,db);
	});
}

/*function called when a request to this route "/shop" is made;
is trying to connect to the database -in case of failure calls showError function to show a specific error message,
otherwise extracts the information needed from the db and then renders index page which is the Home page of our website
*/
exports.index = function(req, res) {
	var mdbClient = require('mongodb').MongoClient;
	connectToDb(function(err, db) {
		if(err) 
			showError(res,"Sorry. We are having trouble right now.Please try again later!");
		else
		{
			var collection = db.collection('categories');
			collection.find().toArray(function(err, items) {
				if(err) 
					showError(res,"Sorry. We are having trouble right now.Please try again later!");
				else
				{
					res.render("index", { 
						// Underscore.js lib
						_     : _, 
						// Template data
						title : "Home",
						currentCategory:"Home",
						ProjectTitle:"Shop",
						Category1:"Mens",
						Category2:"Womens",
						items : items,
						isLogged:req.session.user
					});
					db.close();
				}
			});
		}
	});
};
/*
	Gets the categories from db
	type - category type
	callback - callback function that is called when the blocking operation is finished
*/
function getCategories(type,callback){
	connectToDb(function(err, db) {
		if(err) 
			callback(err);
		else
		{
			var collection = db.collection('categories');
			collection.find({id:type}).toArray(function(err, items) {
				if(err) callback(err);
				else
				{
					callback(null,items);
					db.close();
				}
			});
		}
	});
};
// getSubcategories from database
// type - category type
// subcategory- subcategory name (Clothing, Accessories, etc)
function getSubcategories(type, subcategory,callback)
{
	connectToDb(function(err, db) {
		if(err) 
			callback(err);
		else
		{
			var collection = db.collection('categories');
			collection.find({id:type},{"categories":{$elemMatch: { "id": subcategory}}}).toArray(function(err, items) {
				if(err) callback(err);
				else
				{
					callback(null,items);
					db.close();
				}
			});
		}
	});
};
/*get products from database 
category_id is the primary_category_id of the products to select from db
callback is a function that is called when the blocking operation is finished 
*/
function getProducts(category_id,sort,skip,limit,callback)
{
	connectToDb(function(err, db) {
		if(err)
			callback(err);
		else
		{
			var collection = db.collection('products');
			collection.find({"primary_category_id": category_id})
					  .sort({"price": sort})
					  .skip(skip)
					  .limit(limit)
					  .toArray(function(err, items) {
				if(err)
					callback(err);
				else
				{
					callback(null,items);
					db.close();
				}
			});
		}
	});



};
/*function called when a request to route "/shop/:categ/:subcateg/:products" is made;
is trying to connect to the database -in case of failure calls showError function to show a specific error message,
otherwise extracts the information needed from the db( using request params for queries) and then renders "products_in_categ" page which is the page that displays all products in a specific subcategory
*/
exports.products=function(req,res){
	var sort_criteria=req.query.sort;
	var sort=1;
	/*evaluate sorting criteria received as request parameter and set sort value to use as sort criteria in database data extraction*/ 
	if(sort_criteria=="ascending")
	{
		sort=1;
	}
	else
	{
		sort=-1;
	}
	getProducts(req.params.products,sort,req.query.from*1,limit+1,function(err,items)
	{
		if(err)
			showError(res,"Sorry. We are having trouble right now.Please try again later!");
		else
	{		var showNext=false;
			var showPrev=false;
			if(items.length==limit+1)
			{
				showNext=true;
				items.splice(20,1);
			}
			if(req.query.from!=0)
				showPrev=true;
				
			res.render("products_in_categ", { 
				// Underscore.js lib
				_     : _, 
				// Template data
				title : req.params.categ,
				currentCategory:req.params.categ,
				currentSubcategory:req.params.subcateg,
				currentSubsubcategory:req.params.products,
				reduce_capitalize: reduce_capitalize,
				ProjectTitle:"Shop",
				Category1:"Mens",
				Category2:"Womens",
				limit: limit,
				calculateNext: calculateNext,
				calculatePrev: calculatePrev,
				start: req.query.from*1,
				showNext: showNext,
				showPrev: showPrev,
				items : items,
				isLogged:req.session.user,
				sortingCriteria: req.query.sort
			});
		}
	});
};
/*function called when a request to route "/shop/:categ" is made;
is trying to connect to the database -in case of failure calls showError function to show a specific error message,
otherwise extracts the information needed from the db( using request params for queries) and then renders "categories" page which is the page that displays main categories of products
*/
exports.category= function(req, res) {
	getCategories(req.params.categ,function(err,items){
		if(err)
		{
			showError(res,"Sorry. We are having trouble right now.Please try again later!");
		}
		else
		{
			res.render("categories", { 
				// Underscore.js lib
				_     : _, 
				// Template data
				title : req.params.categ,
				currentCategory:req.params.categ,
				reduce_capitalize: reduce_capitalize,
				ProjectTitle:"Shop",
				Category1:"Mens",
				Category2:"Womens",
				items : items,
				isLogged:req.session.user
			});
		}
		
	});
};
/*function called when a request to route "/shop/:categ/:subcateg" is made;
is trying to connect to the database -in case of failure calls showError function to show a specific error message,
otherwise extracts the information needed from the db( using request params for queries) and then renders "subcateg" page which is the page that displays subactegories of the requested category 
*/
exports.subcategory=function(req,res){
	getSubcategories(req.params.categ,req.params.subcateg,function(err,items){
		if(err)
		{
			showError(res,"Sorry. We are having trouble right now.Please try again later!");
		}
		else
		{
			res.render("subcateg", { 
				// Underscore.js lib
				_     : _, 
				// Template data
				title : req.params.categ,
				currentCategory:req.params.categ,
				currentSubcategory:req.params.subcateg,
				reduce_capitalize: reduce_capitalize,
				ProjectTitle:"Shop",
				Category1:"Mens",
				Category2:"Womens",
				items : items,
				isLogged:req.session.user
			});
		}
			
	});
};

/*function called when a request to route "/shop/:categ/:subcateg/:products/:prod" is made;
is trying to connect to the database -in case of failure calls showError function to show a specific error message,
otherwise extracts the information needed from the db( using request params for queries) and then renders "product" page which is the page that displays extra information about the product (req.params.prod) 
*/
exports.product_description= function(req, res) {
	connectToDb(function(err, db) {
		if(err) 
			showError(res,"Sorry. We are having trouble right now.Please try again later!");
		else
		{
			var collection = db.collection('products');
			collection.find({id:req.params.prod}).toArray(function(err, items) {
				if(err) 
					showError(res,"Sorry. We are having trouble right now.Please try again later!");
				else
				{
					description_len(items[0].page_description,items[0].long_description);
					setDescription(complete_description);
					res.render("product", { 
						// Underscore.js lib
						_     : _, 
						// Template data
						title : "Product Details",
						currentCategory:req.params.categ,
						currentSubcategory:req.params.subcateg,
						currentSubsubcategory:req.params.products,
						reduce_capitalize: reduce_capitalize,
						//currentProduct:req.params.prod,
						ProjectTitle:"Shop",
						Category1:"Mens",
						Category2:"Womens",
						items : items,
						isLogged:req.session.user,
						showMoreInfo: showMoreInfo,
						basic_info: basic_info,
						complete_description: complete_description
					});
					db.close();
				}
			});
		}
	});
};
/*
function called when a request to route /convert is made
integrate node-soap module to use infovalutar service and obtain currency values
infovalutar service require dt( date time) in a specific format and is neccesary to convert lastdateinsertedResult( the date of the latest currency available) to that format 
*/
exports.convert=function(req,res){
	var soap = require('soap');
	var url = 'http://infovalutar.ro/curs.asmx?wsdl';
	var args = {name: 'value'};
	var oldCurrencyValue, //will contain the value in RON of the currency from which we want to convert (req.query.from)
		newCurrencyValue; //will contain the value in RON of the new currency (req.query.to)
	var oldCurrency=req.query.from;
	var newCurrency=req.query.to;
	var currentPrice=req.query.price;
	var newPrice;
	soap.createClient(url, function(err, client) {
		client.lastdateinserted({},function(err,response){
			var date=response.lastdateinsertedResult;
			date=new Date(date);
			var month=((date.getMonth()+1)<10)?"0"+(date.getMonth()+1):(date.getMonth()+1);
			date=date.getFullYear()+"-" +month+ "-" + date.getDate() +"T";
			date=date+"00:00:00";
		    client.getall({dt:date}, function(err, result) {
				/*currency is an array that contains objects from all currencies available (IDMoneda) and their values in RON (Value)*/
				var currency=result.getallResult.diffgram.DocumentElement.Currency;
				//extracts from array only the values for the currency we need 
				for(var i=0;i<currency.length;i++)
				{
					if(currency[i].IDMoneda==oldCurrency)
						oldCurrencyValue=currency[i].Value;
					if(currency[i].IDMoneda==newCurrency)
						newCurrencyValue=currency[i].Value;
				}
				/* if((oldCurrency=='RON') &&(newCurrency!='RON'))
					newPrice=(currentPrice*1)/(newCurrencyValue*1);
				else
				{
					if((oldCurrency!='RON')&&(newCurrency=='RON'))
						newPrice=(oldCurrencyValue*1)*(currentPrice*1);
					else
						newPrice=((oldCurrencyValue*1)*(currentPrice*1))/(newCurrencyValue*1);
				} */
				switch (oldCurrency)
				{
					case 'RON': 
					{
						if(newCurrency!='RON')
						{
							newPrice=(currentPrice*1)/(newCurrencyValue*1);
							break;
						}
					}
					default:
					{
						if(newCurrency=='RON')
						{
							newPrice=(oldCurrencyValue*1)*(currentPrice*1);
						}
						else
						{
							newPrice=((oldCurrencyValue*1)*(currentPrice*1))/(newCurrencyValue*1);
						}
					}
				}
				//send as response the new price converted to the selected currency
				res.send({newPrice:newPrice});
			});
		});
	});
};
/* function called when a request to /orders (button "Place order" is clicked) is made
insert the content of the shopping cart into db , collection orders

*/
exports.orders=function(req,res){
	var mdbClient = require('mongodb').MongoClient;
	if(req.session.user)
	{
		connectToDb(function(err, db) {
			if(err) 
				showError(res,"Sorry. We are having trouble right now.Please try again later!");
			else
			{
				var collection = db.collection('orders');
				/* create object order that will be inserted into db*/
				var date=new Date().toUTCString();
				var order={
					"customer" : req.session.user, 
					"order": req.query.order,
					"date":date,
					"shipping":req.query.shipping
				};
				/* create a string from order and format the string; it will be sent in email as order details to the customer*/
				var orderStr=JSON.stringify(order["order"]);
				orderStr=orderStr.replace(/},/g,'\n');
				orderStr=orderStr.replace(/['{}"]/g,'');
				orderStr=orderStr.replace(/[:]/g,': ');
				/*insert object order in "orders" collection and then send email to the customer with order details*/
				collection.insert(order, function(err,message){
					transporter.sendMail({
						from: 'clothingnew.nodejs@gmail.com',
						to: req.session.user,
						subject: 'order details',
						text: 'Hi, '+'\nThank you for your order!\n'+'Order details: \n'+orderStr+'\n'+ req.query.total
					});
					/*send response that the user is connected and the order succeded*/
					res.send({status:"connected"});
				});
			}
		});
	}
	else
	{
		/* send response that the user is not connected and the order can not be processed before login or signin*/
		res.send({status:"not connected"});
	
	}
};
/* function called when the client press submit button for login form
if the information provided by the client is found in the db that means that the customer has an account, otherwise the customer has to create an account
create new session 
*/
exports.login=function(req,res){
var mdbClient = require('mongodb').MongoClient;
	connectToDb(function(err, db) {
		if(err) 
			showError(res,"Sorry. We are having trouble right now.Please try again later!");
		else
		{
			var collection = db.collection('customers');
			var pass=MD5(req.body.password);
			collection.find({$and:[{email:req.body.email},{password:pass}]}).toArray(function(err,message){
				if(message.length==0)
				{
					res.redirect("/shop#invalidlogin");
				}
				else
				{
					req.session.user=req.body.email;
					res.redirect("/shop");
				}
			});
		}
	});
	
};
/* function called when the client press submit button for sign in form
create new account by inserting information about the customer in the db
send confirmation email to the customer with account details
*/

exports.sign_up=function(req,res){
var MD5=require('MD5');
var mdbClient = require('mongodb').MongoClient;

	connectToDb(function(err, db) {
		if(err) 
			showError(res,"Sorry. We are having trouble right now.Please try again later!");
		else
		{
			var collection = db.collection('customers');
			var password=req.body.password;
			/* apply hash to the password*/
			req.body.password=MD5(req.body.password);
			collection.insert(req.body,function(err,message){
				
				/*send email to the customer with account details*/
				transporter.sendMail({
					from: 'clothingnew.nodejs@gmail.com',
					to: req.body.email,
					subject: 'account create',
					text: 'Hi, '+req.body.firstname+'\nThank you for registration!\n'+'An account was created for you: \n'+'username(email): '+req.body.email+'\n password: '+password
				});
			});
			var collection2 = db.collection('wishlist');
			var wishlist={};
			wishlist["user"]=req.body.email;
			wishlist["products"]=[];
			collection2.insert(wishlist ,function(err,msg){
				if(err)
				{
					showError(res,"Sorry. We are having trouble right now.Please try again later!");
				}
				else
				{
					/*redirect to home page*/
					res.redirect("/shop");
				}
			});
		}
	});
};
/* function called when the client press logout button
delete session and show notification message
*/
exports.logout=function(req,res){
	req.session.destroy();
	if(!req.session){
		res.send({deleted:"deleted"});
	}
};
/*email verification */
exports.email_verification=function(req,res){
	connectToDb(function(err, db) {
		if(err) 
			showError(res,"Sorry. We are having trouble right now.Please try again later!");
		else
		{
			var collection = db.collection('customers');
			collection.find({email:req.body.email}).toArray(function(err, data){
				if(data.length==0)
				{
					res.send({emailStatus:"invalid"});
				}
				else
				{
					res.send({emailStatus:"valid"});
				}	
			});
		}
	});
};
/* reset password */
exports.reset_password=function(req,res){
	var newPassword=req.body.email.split('@')[0];
	transporter.sendMail({
		from: 'clothingnew.nodejs@gmail.com',
		to: req.body.email,
		subject: 'reset password',
		text: 'Hi,\nA password reset was requested for this account\n'+'New password:'+newPassword
	});
	connectToDb(function(err, db) {
		if(err) 
			showError(res,"Sorry. We are having trouble right now.Please try again later!");
		else
		{
			var collection = db.collection('customers');
			collection.update({email:req.body.email},{$set:{password:MD5(newPassword)}},function(err){
			
			});
		}
	});
	res.redirect("/shop");
};
/*show order history*/
exports.order_history=function(req,res){
	connectToDb(function(err, db) {
		if(err) 
			showError(res,"Sorry. We are having trouble right now.Please try again later!");
		else
		{
			var collection = db.collection('orders');
			collection.find({customer:req.session.user}).toArray(function(err,items){
				if(err)
				{
					showError(res,"Sorry. We are having trouble right now.Please try again later!");
				}
				else
				{
					res.send({history:items});
				}
			});
		}
	});
};
/*show wishlist*/
exports.wishlist=function(req,res){
	connectToDb(function(err, db) {
		if(err) 
			showError(res,"Sorry. We are having trouble right now.Please try again later!");
		else
		{
			var collection = db.collection('wishlist');
			collection.find({user:req.session.user}).toArray(function(err,items){
				if(err)
				{
					showError(res,"Sorry. We are having trouble right now.Please try again later!");
				}
				else
				{
					res.send({wishlist:items});
				}
			});
		}
	});
};
/*add product to wishlist by inserting product in wishlist collection*/
exports.addToWishlist=function(req,res){
	connectToDb(function(err, db) {
		if(err) 
			showError(res,"Sorry. We are having trouble right now.Please try again later!");
		else
		{
			var collection = db.collection('wishlist');
			var product={};
			product["name"]=req.query.product;
			product["price"]=req.query.price;
			collection.update(
				{ user: req.session.user},
				{ $push: { products: product} } 
			,function(err,msg){
				if(err)
				{
					showError(res,"Sorry. We are having trouble right now.Please try again later!");
				}
				else
				{
					res.send({result:"ok"});
				}
					
			
			});
		}
	});
};
/*remove product from wishlist, delete entry from db*/
exports.remove_wishlist=function(req,res){
	connectToDb(function(err, db) {
		if(err) 
			showError(res,"Sorry. We are having trouble right now.Please try again later!");
		else
		{
			var collection = db.collection('wishlist');
			collection.update(
				{ user: req.session.user},
				{ $pull: { products: {name: req.query.name } }}
			,function(err,msg){
				if(err)
				{
					showError(res,"Sorry. We are having trouble right now.Please try again later!");
				}
				else
				{
					res.send({result:"ok"});
				}
			
			});
		}
	});
};
exports.cardPayment=function(req,res){
	res.redirect("/shop");


};